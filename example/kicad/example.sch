EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Simulation_SPICE:VDC V3
U 1 1 603D759E
P 5250 1250
F 0 "V3" H 5380 1341 50  0000 L CNN
F 1 "VDC" H 5380 1250 50  0000 L CNN
F 2 "" H 5250 1250 50  0001 C CNN
F 3 "~" H 5250 1250 50  0001 C CNN
F 4 "Y" H 5250 1250 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 5250 1250 50  0001 L CNN "Spice_Primitive"
F 6 "dc(1)" H 5380 1159 50  0000 L CNN "Spice_Model"
	1    5250 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 603D80E1
P 5250 750
F 0 "R11" H 5320 796 50  0000 L CNN
F 1 "1" H 5320 705 50  0000 L CNN
F 2 "" V 5180 750 50  0001 C CNN
F 3 "~" H 5250 750 50  0001 C CNN
F 4 "R" H 5250 750 50  0001 C CNN "Spice_Primitive"
F 5 "1k" H 5250 750 50  0001 C CNN "Spice_Model"
F 6 "Y" H 5250 750 50  0001 C CNN "Spice_Netlist_Enabled"
	1    5250 750 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 603D885E
P 5900 1350
F 0 "R12" H 5830 1304 50  0000 R CNN
F 1 "10" H 5830 1395 50  0000 R CNN
F 2 "" V 5830 1350 50  0001 C CNN
F 3 "~" H 5900 1350 50  0001 C CNN
	1    5900 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5250 1450 5250 1550
Wire Wire Line
	5250 1550 5900 1550
Wire Wire Line
	5900 1550 5900 1500
Wire Wire Line
	5900 1200 5900 500 
Wire Wire Line
	5900 500  5250 500 
Wire Wire Line
	5250 500  5250 600 
Wire Wire Line
	5250 900  5250 1050
Text Label 5250 1000 0    50   ~ 0
vin
Text Label 5900 700  0    50   ~ 0
vout
Text Label 5450 1550 0    50   ~ 0
0
$EndSCHEMATC
