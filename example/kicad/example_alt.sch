EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R5
U 1 1 603C963F
P 4800 2950
F 0 "R5" V 4593 2950 50  0000 C CNN
F 1 "1k" V 4684 2950 50  0000 C CNN
F 2 "" V 4730 2950 50  0001 C CNN
F 3 "~" H 4800 2950 50  0001 C CNN
	1    4800 2950
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 603C9B91
P 4800 3400
F 0 "R7" V 4593 3400 50  0000 C CNN
F 1 "1k" V 4684 3400 50  0000 C CNN
F 2 "" V 4730 3400 50  0001 C CNN
F 3 "~" H 4800 3400 50  0001 C CNN
	1    4800 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 603CBB21
P 4400 3200
F 0 "R6" H 4470 3246 50  0000 L CNN
F 1 "0.1R" H 4470 3155 50  0000 L CNN
F 2 "" V 4330 3200 50  0001 C CNN
F 3 "~" H 4400 3200 50  0001 C CNN
	1    4400 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 603CD8DB
P 3550 3450
F 0 "R8" H 3620 3496 50  0000 L CNN
F 1 "10R" H 3620 3405 50  0000 L CNN
F 2 "" V 3480 3450 50  0001 C CNN
F 3 "~" H 3550 3450 50  0001 C CNN
	1    3550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 4000 3550 4000
Wire Wire Line
	3550 4000 3550 3600
$Comp
L Device:C C1
U 1 1 603D1D72
P 7700 2000
F 0 "C1" V 7448 2000 50  0000 C CNN
F 1 "10n" V 7539 2000 50  0000 C CNN
F 2 "" H 7738 1850 50  0001 C CNN
F 3 "~" H 7700 2000 50  0001 C CNN
	1    7700 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 603D3723
P 7050 1750
F 0 "R3" H 7120 1796 50  0000 L CNN
F 1 "50k" H 7120 1705 50  0000 L CNN
F 2 "" V 6980 1750 50  0001 C CNN
F 3 "~" H 7050 1750 50  0001 C CNN
	1    7050 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 603D42B8
P 6750 1750
F 0 "R2" H 6680 1704 50  0000 R CNN
F 1 "50k" H 6680 1795 50  0000 R CNN
F 2 "" V 6680 1750 50  0001 C CNN
F 3 "~" H 6750 1750 50  0001 C CNN
	1    6750 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 3500 5450 3650
Wire Wire Line
	5550 3500 5850 3500
Wire Wire Line
	5850 3500 5850 3650
Text Label 6450 3200 0    50   ~ 0
vout
Wire Wire Line
	4400 3050 4400 2950
Wire Wire Line
	4400 2950 4650 2950
Wire Wire Line
	4400 3350 4400 3400
Wire Wire Line
	4400 3400 4650 3400
Wire Wire Line
	4950 3400 5050 3400
Wire Wire Line
	5150 3400 5150 3300
Wire Wire Line
	5150 3100 5150 2950
Wire Wire Line
	5150 2950 5050 2950
$Comp
L Device:R R1
U 1 1 603D7D7E
P 7050 1350
F 0 "R1" H 7120 1396 50  0000 L CNN
F 1 "22k" H 7120 1305 50  0000 L CNN
F 2 "" V 6980 1350 50  0001 C CNN
F 3 "~" H 7050 1350 50  0001 C CNN
	1    7050 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 1200 7050 1000
Wire Wire Line
	7050 1600 7050 1550
Connection ~ 7050 1550
Wire Wire Line
	7050 1550 7050 1500
Wire Wire Line
	7050 1900 7050 2000
Text Label 7350 2000 0    50   ~ 0
vcb
Text Label 5700 3500 0    50   ~ 0
vcomp
Wire Wire Line
	4400 2950 3550 2950
Connection ~ 4400 2950
Wire Wire Line
	3550 3300 3550 2950
Connection ~ 3550 2950
Wire Wire Line
	2800 3200 2800 2950
Wire Wire Line
	2800 2950 3550 2950
$Comp
L Simulation_SPICE:ISIN I1
U 1 1 603EEDC6
P 2800 3400
F 0 "I1" H 2930 3491 50  0000 L CNN
F 1 "ISIN" H 2930 3400 50  0000 L CNN
F 2 "" H 2800 3400 50  0001 C CNN
F 3 "~" H 2800 3400 50  0001 C CNN
F 4 "Y" H 2800 3400 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "I" H 2800 3400 50  0001 L CNN "Spice_Primitive"
F 6 "sin(0 10n 1k)" H 2930 3309 50  0000 L CNN "Spice_Model"
	1    2800 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 4000 2800 3600
Wire Wire Line
	4400 3400 4400 4000
Wire Wire Line
	4400 4000 3550 4000
Connection ~ 4400 3400
Connection ~ 3550 4000
$Comp
L Simulation_SPICE:VDC V1
U 1 1 603F31C2
P 1800 1600
F 0 "V1" H 1930 1691 50  0000 L CNN
F 1 "VDC" H 1930 1600 50  0000 L CNN
F 2 "" H 1800 1600 50  0001 C CNN
F 3 "~" H 1800 1600 50  0001 C CNN
F 4 "Y" H 1800 1600 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 1800 1600 50  0001 L CNN "Spice_Primitive"
F 6 "dc(36)" H 1930 1509 50  0000 L CNN "Spice_Model"
	1    1800 1600
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:VDC V2
U 1 1 603F6209
P 2450 1600
F 0 "V2" H 2580 1691 50  0000 L CNN
F 1 "VDC" H 2580 1600 50  0000 L CNN
F 2 "" H 2450 1600 50  0001 C CNN
F 3 "~" H 2450 1600 50  0001 C CNN
F 4 "Y" H 2450 1600 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2450 1600 50  0001 L CNN "Spice_Primitive"
F 6 "dc(-36)" H 2580 1509 50  0000 L CNN "Spice_Model"
	1    2450 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1800 1800 1900
Wire Wire Line
	2450 1900 2450 1800
Wire Wire Line
	1800 1250 1800 1400
Wire Wire Line
	2450 1250 2450 1400
$Comp
L Device:R R9
U 1 1 6040146D
P 7700 3550
F 0 "R9" H 7770 3596 50  0000 L CNN
F 1 "10k" H 7770 3505 50  0000 L CNN
F 2 "" V 7630 3550 50  0001 C CNN
F 3 "~" H 7700 3550 50  0001 C CNN
	1    7700 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3850 7700 3700
Wire Wire Line
	7700 3400 7700 3200
Wire Wire Line
	5750 3200 6300 3200
Wire Notes Line
	7000 2900 7000 4300
Wire Notes Line
	7000 4300 8300 4300
Wire Notes Line
	8300 4300 8300 2900
Wire Notes Line
	8300 2900 7000 2900
Wire Notes Line
	3800 4350 2250 4350
Wire Notes Line
	2250 4350 2250 2400
Wire Notes Line
	2250 2400 3800 2400
Wire Notes Line
	3800 2400 3800 4350
Text Notes 2400 2550 0    50   ~ 0
*Previous Stage
Text Notes 7200 3050 0    50   ~ 0
*Subsequent Stage
Wire Wire Line
	5450 2900 5450 2800
Wire Wire Line
	5450 2800 5600 2800
Text Label 5600 2800 0    50   ~ 0
vcb
Wire Wire Line
	6750 1600 6750 1550
Wire Wire Line
	6750 1550 7050 1550
Wire Wire Line
	6750 1900 6750 2000
Wire Wire Line
	6750 2000 6900 2000
Wire Wire Line
	5450 3650 5650 3650
Text Label 5450 3650 0    50   ~ 0
vbal
Text Label 6800 2000 0    50   ~ 0
vbal
Wire Wire Line
	7050 2000 7550 2000
Text Label 8100 2000 0    50   ~ 0
vcomp
Wire Wire Line
	7850 2000 8100 2000
Wire Notes Line
	3800 900  3800 2300
Wire Notes Line
	3800 2300 1550 2300
Wire Notes Line
	1550 2300 1550 900 
Wire Notes Line
	1600 900  1600 950 
Wire Notes Line
	1550 900  3800 900 
Text Notes 2850 1100 0    50   ~ 0
*Power Supplies
Wire Notes Line
	6600 2200 6600 650 
Wire Notes Line
	6600 650  8750 650 
Wire Notes Line
	8750 650  8750 2150
Wire Notes Line
	8750 2150 6600 2150
Text Notes 7200 750  0    50   ~ 0
* Compensation and Offset-Balancing
$Comp
L Device:R R4
U 1 1 60475D70
P 5400 2400
F 0 "R4" V 5193 2400 50  0000 C CNN
F 1 "100k" V 5284 2400 50  0000 C CNN
F 2 "" V 5330 2400 50  0001 C CNN
F 3 "~" H 5400 2400 50  0001 C CNN
	1    5400 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 2800 5350 2900
$Comp
L Device:R R10
U 1 1 6047A43E
P 5400 4100
F 0 "R10" V 5193 4100 50  0000 C CNN
F 1 "100k" V 5284 4100 50  0000 C CNN
F 2 "" V 5330 4100 50  0001 C CNN
F 3 "~" H 5400 4100 50  0001 C CNN
	1    5400 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 4100 5050 4100
Wire Wire Line
	5050 4100 5050 3400
Connection ~ 5050 3400
Wire Wire Line
	5050 3400 5150 3400
Wire Wire Line
	5050 2950 5050 2400
Wire Wire Line
	5050 2400 5250 2400
Connection ~ 5050 2950
Wire Wire Line
	5050 2950 4950 2950
Wire Wire Line
	5550 2400 6300 2400
Wire Wire Line
	6300 2400 6300 3200
Connection ~ 6300 3200
Wire Wire Line
	6300 3200 7700 3200
Wire Wire Line
	5550 4100 6300 4100
Wire Wire Line
	6300 4100 6300 3200
Text Label 5050 2400 0    50   ~ 0
vin_p
Text Label 5050 4100 0    50   ~ 0
vin_n
Wire Notes Line
	4400 4250 6900 4250
Wire Notes Line
	6900 4250 6900 5750
Wire Notes Line
	6900 5750 4400 5750
Wire Notes Line
	4400 5750 4400 4250
Text Notes 4650 4450 0    50   ~ 0
*Simulator Commands
Text Label 2450 1250 0    50   ~ 0
VEE
Text Label 1800 1250 0    50   ~ 0
VCC
Wire Wire Line
	5350 2800 5200 2800
Text Label 5200 2800 0    50   ~ 0
VCC
Text Label 5200 3800 0    50   ~ 0
VEE
Wire Wire Line
	5150 3800 5350 3800
Wire Wire Line
	5350 3500 5350 3800
Wire Wire Line
	7050 1000 7200 1000
Text Label 7050 1000 0    50   ~ 0
VCC
$Comp
L Simulation_SPICE:OPAMP U?
U 1 1 603DB458
P 5450 3200
F 0 "U?" H 5794 3246 50  0000 L CNN
F 1 "OPAMP" H 5794 3155 50  0000 L CNN
F 2 "" H 5450 3200 50  0001 C CNN
F 3 "~" H 5450 3200 50  0001 C CNN
F 4 "Y" H 5450 3200 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "X" H 5450 3200 50  0001 L CNN "Spice_Primitive"
F 6 "OPAMP" H 5450 3200 50  0001 C CNN "Spice_Model"
F 7 "1 2 4 5 3" H 5450 3200 50  0001 C CNN "Spice_Node_Sequence"
F 8 "/home/bjmuld/work/ngspice-tutorial/example/741.lib" H 5450 3200 50  0001 C CNN "Spice_Lib_File"
	1    5450 3200
	1    0    0    -1  
$EndComp
Text Label 2800 2950 0    50   ~ 0
vin
Text Label 4050 4000 0    50   ~ 0
0
Wire Wire Line
	1800 1900 2450 1900
Text Label 7700 3850 0    50   ~ 0
0
Text Label 2200 1900 0    50   ~ 0
0
$EndSCHEMATC
